// Required for Django CSRF
function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    };

// Actual Upload function using xhr
function upload(blob){
    //console.log('en upload?');
        var csrftoken = getCookie('csrftoken');

        var xhr = new XMLHttpRequest();
        xhr.open('POST', '', true);
        xhr.setRequestHeader("X-CSRFToken", csrftoken);

        var form = document.querySelector('form')

        // get all other field in the form
        var fd = new FormData(form)

        // append the needed blob to the formdata
        fd.append('blob', blob, 'audio_va.ogg');

        xhr.upload.onloadend = function() {
            //alert('Upload complete??');
        };

        xhr.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 302) {
                    var json = JSON.parse(this.responseText);
                    console.log(json.success);
                    //following line would actually change the url of your window.
                    window.location.href = json.success;
                }
            };

        xhr.send(fd);
};

var controller = (function() {
  var DOMStrngs = {
      audioQuest: '.AudioTypeSelection',
      recordModule: '.RecordingModule'
  };

  var clearInit = function() {
      // Hide upload and submit options
      var uploadFile = document.getElementById('id_audio_file');
      var submitBtn = document.getElementById('submitInput');
      uploadFile.parentNode.parentNode.style.display = 'none';
      submitBtn.parentNode.style.display = 'none';
      document.querySelector(DOMStrngs.recordModule).parentNode.style.display = 'none';
  };

  var clearQuestion = function() {
      var quest = document.querySelector(DOMStrngs.audioQuest);
      quest.parentNode.style.display = 'none'
  }

  var setupEventListeners = function (arguments) {
      document.getElementById('grabarInput').addEventListener('click', function(e) {
          clearQuestion();
          document.querySelector(DOMStrngs.recordModule).parentNode.style.display = 'grid';

          function screenLogger(text, data) {
          //log.innerHTML += "\n" + text + " " + (data || '');
          }

          if (!Recorder.isRecordingSupported()) {
              screenLogger("Recording features are not supported in your browser.");
              console.log("Recording features are not supported in your browser.");
              alert('Tu explorador no permite grabaciones. :(');
              //TODO disablerecording options
          } else {
              var start = document.getElementById('start');
              var stopBtn = document.getElementById('stopButton');
              //start.disabled = false;
              //start.style.fill = 'black';

              var options = {
                  monitorGain: 0,
                  recordingGain: 1,
                  numberOfChannels: 1,
                  encoderSampleRate: 48000,
                  encoderPath: encoderPathT
              };

              var recorder = new Recorder(options);
              //pause.addEventListener( "click", function(){ recorder.pause(); });
              //resume.addEventListener( "click", function(){ recorder.resume(); });
              stopBtn.addEventListener( "click", function(){
                  recorder.stop();
                  console.log('¿stopping?');
              });
              start.addEventListener( "click", function(){
                  recorder.start().catch(function(e){
                    screenLogger('Error encountered:', e.message );
                  });
              });

              recorder.onstart = function(e){
                  console.log('Recorder is started');
                  start.style.display = 'none';
                  stopBtn.style.display = 'block';
                  stopBtn.disabled = false;
                  //stopBtn.style.fill = 'black';
                  recLabel.style.display = 'block';
              };

              recorder.onstop = function(e){
                  console.log('Recorder is stopped');
                  start.disabled = true;
                  start.style.fill = 'gray';
                  stopBtn.disabled = true;
                  stopBtn.style.display = 'none';
                  //stopBtn.style.fill = 'gray';
                  recLabel.style.display = 'none';
              };

              recorder.ondataavailable = function( typedArray ){
                  var dataBlob = new Blob( [typedArray], { type: 'audio/wav' } );
                  var fileName = new Date().toISOString() + ".wav";
                  var url = URL.createObjectURL( dataBlob );

                  var audio = document.createElement('audio');
                  audio.controls = true;
                  audio.src = url;

                  var submitBtn = document.createElement('button');
                  submitBtn.innerHTML = "Enviar";
                  submitBtn.id = 'submitGrabacion';
                  submitBtn.classList.add("button");
                  submitBtn.classList.add("button-send");

                  var button = document.createElement('button');
                  button.innerHTML = "&#8634; Borrar y volver a grabar";
                  button.id = 'reGrabar';
                  button.classList.add("button");
                  button.classList.add("button-send");
                  submitBtn.onclick = function(e) {
                      //console.log('enviando Form?');
                      e.preventDefault();
                      var form = document.getElementById('myForm');
                      if (form.checkValidity()) {
                          upload(dataBlob);
                          //console.log('form valida?');
                      } else {
                          form.reportValidity();
                      }
                  };
                  // On deletel btn click
                  button.onclick = function() {
                      // delete current recording
                      // enable recording again.
                      start.style.display = 'block';
                      start.disabled = false;
                      start.style.fill = 'black';
                      recordingslist.removeChild(recordingslist.childNodes[0]);
                  }

                  var li = document.createElement('li');
                  li.appendChild(audio);
                  li.appendChild(submitBtn);
                  li.appendChild(button);

                  recordingslist.appendChild(li);
              };
          }
      })
      document.getElementById('subirInput').addEventListener('click', function(e) {
          clearQuestion();
          var uploadFile = document.getElementById('id_audio_file');
          var submitBtn = document.getElementById('submitInput');
          uploadFile.parentNode.parentNode.style.display = 'grid';
          submitBtn.parentNode.style.display = 'grid';
      })
  };

  return {
      init: function() {
          clearInit();
          setupEventListeners();
          //
      }
  }
})();

controller.init();
