var backdrop = document.querySelector('.backdrop');
var mapinfo = document.querySelector('.main-mapinfo');
var closemapinfoButton = document.querySelector('.main-mapinfo__close');
var mainmapMV = document.querySelector('.main-map');
var stateOnMap = document.querySelectorAll('.landNoData');
var jsonData;
var selState;
var selStateX;
var selVideo1 = "";
var selVideo2 = "";
var selVideo3 = "";
var videoAcentoButton = document.querySelectorAll('.main-mapinfo .button');
var modal = document.querySelector('.modal');

// extracting JSON info of all states
fetch('/ajax/').then(response => {
  return response.json();
}).then(data => {
  jsonData = data;
})
.then(stateUpdate)
.catch(err => {
   alert('Json extracting error: ' + err);
});

function stateUpdate () {

    for (var i = 0; i < stateOnMap.length; i++) {

        // Update color of states if Data available
        selStateX = jsonData.filter(obj => {
          return obj.state_mexico === stateOnMap[i].id
        });
        if (selStateX.length > 0) {
            stateOnMap[i].classList.remove('landNoData');
            stateOnMap[i].classList.add('land');
        }

        //Showing Info of state when clicking on Map
      stateOnMap[i].addEventListener ('click', function(){
          //TODO: ¿Duplicated filter?
          selState = jsonData.filter(obj => {
            return obj.state_mexico === this.id
          });
          console.log('en Event Listener');
          console.log(selState[0]);
          //TODO: eliminate try for ifs
        try {
            selVideo1 = selState[0].tags[0][0];
            document.getElementById('statename').innerHTML = this.getAttribute("title");
            document.getElementById('stateinfo').innerHTML = selState[0].state_description;
            console.log(selState[0].tags[0][0]);
            document.getElementById('videoName1').innerHTML = selState[0].tags[0][1];
            mapinfo.classList.add('open');
            mainmapMV.classList.add('main-map__move');
        } catch (e) {

        }
        try {
            document.getElementById('videoName2').innerHTML = selState[0].tags[1][1];
            document.getElementById('videoName2').style.display = "inline";
            document.getElementById('videoName2LI').style.display = "list-item";
            selVideo2 = selState[0].tags[1];
        } catch (e) {
            document.getElementById('videoName2').style.display = "none";
            document.getElementById('videoName2LI').style.display = "none";
        }
        try {
            document.getElementById('videoName3').innerHTML = selState[0].tags[2][1];
            document.getElementById('videoName3').style.display = "inline";
            document.getElementById('videoName3LI').style.display = "list-item";
        } catch (e) {
            document.getElementById('videoName3').style.display = "none";
            document.getElementById('videoName3LI').style.display = "none";
        }
      });
    };
};


//Closing Map Info banner
closemapinfoButton.addEventListener('click', closeBanner);

function closeBanner (arguments) {
    mapinfo.classList.remove('open');
    mainmapMV.classList.remove('main-map__move');
};

//Showing Video of Acentos
for (var i = 0; i < videoAcentoButton.length; i++) {
    videoAcentoButton[i].addEventListener ('click', function(){
        if (this.id == "videoName1") {
            document.getElementById('statevideo').src = selVideo1;
        } else if (this.id == "videoName2") {
            document.getElementById('statevideo').src = selVideo2;
        } else if (this.id == "videoName3") {
            document.getElementById('statevideo').src = selVideo3;
        } else {
            console.log("Error");
        }
        modal.classList.add('open');
        backdrop.classList.add('open');
    });
};

//Closing Video of acentos
backdrop.addEventListener('click', function(){
    modal.classList.remove('open');
    mainmapMV.classList.remove('main-map__move');
    backdrop.classList.remove('open');
    // Stop youtube video by refresging source.
    var el_src = document.getElementById('statevideo').src
    document.getElementById('statevideo').src = el_src;
});
