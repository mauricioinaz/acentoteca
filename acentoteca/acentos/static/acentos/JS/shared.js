var hamburguerButton = document.querySelector('.main-header__hamburguer-btn');
var closeHmbButton = document.querySelector('.mobile-menu__close');
var mobileNav = document.querySelector('.mobile-menu');


//To activate mobile menu
hamburguerButton.addEventListener ('click', function(){
    mobileNav.classList.add('open');
});

closeHmbButton.addEventListener('click', function(){
    mobileNav.classList.remove('open');
});
