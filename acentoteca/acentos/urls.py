from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('ajax/', views.get_state_info, name='ajax'),
    path('acerca-de/', views.acerca_de, name='acerca_de'),
    path('explora-acentos/', views.explora_acentos, name='explora_acentos'),
    path('explora-lista-acentos/',
         views.explora_lista_acentos, name='explora_lista_acentos'),
    path('acentos-json/', views.acentos_json, name='acentos_json'),
    path('acento-nuevo/', views.acento_nuevo, name='acento_nuevo'),
    path('acento-nuevo-valido/',
         views.acento_nuevo_valido, name='acento_nuevo_valido'),
]
