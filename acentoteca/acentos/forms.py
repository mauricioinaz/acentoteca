from django import forms
from .models import Acentos
from leaflet.forms.fields import PointField


class AcentoForm(forms.ModelForm):
    location = PointField()

    class Meta:
        model = Acentos
        fields = ('name', 'state_mexico', 'description',
                  'location', 'user_email', 'audio_file'
                  )
