from django.db import models
from django.contrib.gis.db import models as models2
from django.db.models import Manager as GeoManager
from embed_video.fields import EmbedVideoField
from django.utils import timezone
from django.core.validators import FileExtensionValidator
from django.core.exceptions import ValidationError


AUDIO_FILE_TYPES = [
    'ogg', 'mp3', '3gp', 'm4a', 'aac', 'mp4', 'wav',
    'wma', 'mp2', 'au', 'wma', 'aiff', 'aif', 'snd']


def validate_size(obj):
    file_size = obj.file.size
    limit_mb = 12
    if file_size > limit_mb * 1024 * 1024:
        raise ValidationError(
            "Sólo podemos recibir archivos menores a %s MB" % limit_mb)


class Acentos(models.Model):
    MEXICO_STATES = (
        ('AGU', 'Aguascalientes'),
        ('BCN', 'Baja California'),
        ('BCS', 'Baja California Sur'),
        ('CAM', 'Campeche'),
        ('CHP', 'Chiapas'),
        ('CHH', 'Chihuahua'),
        ('CDM', 'Ciudad de México'),
        ('COA', 'Coahuila'),
        ('COL', 'Colima'),
        ('DUR', 'Durango'),
        ('GUA', 'Guanajuato'),
        ('GRO', 'Guerrero'),
        ('HID', 'Hidalgo'),
        ('JAL', 'Jalisco'),
        ('MEX', 'Estado de México'),
        ('MIC', 'Michoacán'),
        ('MOR', 'Morelos'),
        ('NAY', 'Nayarit'),
        ('NLE', 'Nuevo León'),
        ('OAX', 'Oaxaca'),
        ('PUE', 'Puebla'),
        ('QUE', 'Querétaro'),
        ('ROO', 'Quintana Roo'),
        ('SLP', 'San Luis Potosí'),
        ('SIN', 'Sinaloa'),
        ('SON', 'Sonora'),
        ('TAB', 'Tabasco'),
        ('TAM', 'Tampico'),
        ('TLA', 'Tlaxcala'),
        ('VER', 'Veracruz'),
        ('YUC', 'Yucatán'),
        ('ZAC', 'Zacatecas'),)
    name = models.CharField(
        max_length=40, verbose_name='Acento',
        help_text=' Nombre de tu acento')
    state_mexico = models.CharField(
        max_length=25, choices=MEXICO_STATES, default='AGU',
        verbose_name='Estado')
    description = models.TextField(
        max_length=300,
        default="",
        verbose_name=" Describe tu acento",
        help_text=' Describe tu acento')
    location = models2.PointField(srid=4326, verbose_name='Ubicación')
    video = EmbedVideoField(
        verbose_name='Video o audio',
        help_text=' youtube.com/...   soundcloud.com/...'
        '   vimeo.com/...',
        default="", blank=True)
    objects = GeoManager()
    audio_file = models.FileField(
        upload_to="audio", null=True, blank=True,
        validators=[FileExtensionValidator(AUDIO_FILE_TYPES), validate_size],
        verbose_name='Archivo de Audio')
    user_email = models.EmailField(
        verbose_name="Correo Electrónico",
        help_text=' Tu email nos servirá para avisarte'
                  ' cuando tu archivo se haya subido')
    created_date = models.DateTimeField(default=timezone.now)
    published_revision = models.BooleanField(
        default=False, verbose_name='Publicar Acento')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Acento'
        verbose_name_plural = "Acentos"


# TEMPORAL UNTIL POSTGIS IS INSTALLED
class Ubicacion(models.Model):
    acento = models.OneToOneField(
        Acentos, on_delete=models.CASCADE, primary_key=True,)
    srid = 4326
    x = models.FloatField(default=0, null=True, blank=True)
    y = models.FloatField(default=0, null=True, blank=True)

    def __str__(self):
        # return "la ubicación de %s" % (self.acento)
        return "SRID=%s;POINT (%s %s)" % (self.srid, self.x, self.y)


class Home_states(models.Model):
    state_mexico = models.CharField(
        max_length=25, choices=Acentos.MEXICO_STATES,
        unique=True, verbose_name='Estado')
    state_description = models.CharField(
        max_length=120,
        blank=True,
        help_text="Descripción del Estado",
        verbose_name='Descripción')
    tags = models.ManyToManyField(
        Acentos, blank=True,
        verbose_name='Acento a desplegar')

    class Meta:
        verbose_name = 'Estado'
        verbose_name_plural = 'Información por Estados'


class Home_contents(models.Model):
    HOME_PAGES_TITLES = (
        ('ACD', 'Acerca de'),
        ('ANE', 'Acento Nuevo Enviado'),
        ('ANV', 'Acento Nuevo'),)
    title = models.CharField(
        max_length=20, choices=HOME_PAGES_TITLES,
        unique=True, verbose_name='Título')
    content = models.TextField(help_text="...contenido...")

    class Meta:
        verbose_name = 'contenido'
        verbose_name_plural = 'Contenidos de las páginas'
