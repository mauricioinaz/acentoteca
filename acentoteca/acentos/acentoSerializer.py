from rest_framework import serializers, status
from .models import Acentos, Ubicacion


# TEMPORAL UNTIL POSTGIS IS INSTALLED
class UbicacionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Ubicacion
        fields = ('srid', 'x', 'y')


class AcentoSerializer(serializers.ModelSerializer):
    """
    A student serializer to return the student details
    """
    ubicacion = UbicacionSerializer(required=True)

    class Meta:
        model = Acentos
        fields = ('name', 'description', 'audio_file', 'ubicacion',)
