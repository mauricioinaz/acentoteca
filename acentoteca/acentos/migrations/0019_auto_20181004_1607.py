# Generated by Django 2.0.3 on 2018-10-04 16:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acentos', '0018_auto_20181004_1544'),
    ]

    operations = [
        migrations.AlterField(
            model_name='acentos',
            name='user_email',
            field=models.EmailField(default='fulano@mail.com', help_text='Tu email nos servirá para avisarte cuando tu archivo se haya subido', max_length=254, verbose_name='Correo Electrónico'),
        ),
    ]
