# Generated by Django 2.0.3 on 2018-07-17 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('acentos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='acentos',
            name='state_mexico',
            field=models.CharField(choices=[('AGU', 'Aguascalientes'), ('BCN', 'Baja California'), ('BCS', 'Baja California Sur'), ('CAM', 'Campeche'), ('CHP', 'Chiapas'), ('CHH', 'Chihuahua'), ('CDM', 'Ciudad de México'), ('COA', 'Coahuila'), ('COL', 'Colima'), ('DUR', 'Durango'), ('GUA', 'Guanajuato'), ('GRO', 'Guerrero'), ('HID', 'Hidalgo'), ('JAL', 'Jalisco'), ('MEX', 'Estado de México'), ('MIC', 'Michoacán'), ('MOR', 'Morelos'), ('NAY', 'Nayarit'), ('NLE', 'Nuevo León'), ('PUE', 'Puebla'), ('QUE', 'Querétaro'), ('ROO', 'Quintana Roo'), ('SLP', 'San Luis Potosí'), ('SIN', 'Sinaloa'), ('SON', 'Sonora'), ('TAB', 'Tabasco'), ('TAM', 'Tampico'), ('TLA', 'Tlaxcala'), ('VER', 'Veracruz'), ('YUC', 'Yucatán'), ('ZAC', 'Zacatecas')], default='JAL', max_length=25),
        ),
        migrations.AlterField(
            model_name='acentos',
            name='name',
            field=models.CharField(max_length=40),
        ),
    ]
