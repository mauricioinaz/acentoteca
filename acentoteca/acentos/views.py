from django.shortcuts import render, redirect
from .models import Home_states, Home_contents, Acentos
from django.core.serializers import serialize
from django.http import HttpResponse, JsonResponse
from embed_video.backends import detect_backend, UnknownBackendException
from .forms import AcentoForm
import json
from django.core.files.base import ContentFile
from django.utils.datastructures import MultiValueDictKeyError
from django.urls import reverse
from .acentoSerializer import AcentoSerializer
from rest_framework.renderers import JSONRenderer


def home(request):
    return render(request, 'acentos/index.html')


def acerca_de(request):
    acerca_contenido = Home_contents.objects.get(title='ACD')
    return render(
        request, 'acentos/acerca-de.html',
        {'acerca_de': acerca_contenido})


def get_state_info(request):
    raw_data = serialize('python', Home_states.objects.all())
    actual_data = [d['fields'] for d in raw_data]
    # insert video links in json data
    videos = {
        j.id: (get_video_link(j.video), j.name)
        for i in Home_states.objects.all() for j in i.tags.all()
    }
    for i, data_item in enumerate(actual_data):
        actual_data[i]['tags'] = [videos[x] for x in data_item['tags']]
    return HttpResponse(
        json.dumps(list(actual_data)),
        content_type='application/json; charset=utf8')


def get_video_link(video):
    try:
        return detect_backend(video).url
        # return detect_backend(video).pattern_url.format(
        #    protocol='https', code=detect_backend(video).code)
    except UnknownBackendException:
        return ''


def explora_acentos(request):
    return render(request, 'acentos/explora-acentos.html')


def acentos_json(request):
    # acentos_j = serialize(
    #     'json', Acentos.objects.filter(published_revision=True))
    # TEMPORAL UNTIL POSTGIS IS INSTALLED
    acentos_j = AcentoSerializer(
        Acentos.objects.filter(published_revision=True), many=True)
    # return HttpResponse(acentos_j.data, content_type='json')
    # TODO: Check if correct usage of render
    return HttpResponse(JSONRenderer().render(acentos_j.data))


def explora_lista_acentos(request):
    # Separate Acentos by State
    st_list = {
        state[1]: Acentos.objects.filter(state_mexico=state[0])
        .filter(published_revision=True)
        for state in Acentos.MEXICO_STATES
    }
    return render(request, 'acentos/explora-lista-acentos.html',
                  {'st_list': st_list})


def acento_nuevo(request):
    acento_nvo = Home_contents.objects.get(title='ANV')
    if request.method == "POST":
        form = AcentoForm(request.POST, request.FILES)
        if form.is_valid():
            obj = form.save()
            # extract recorded audio from Blob
            try:
                file = request.FILES['blob']
                obj.audio_file.save('acento_grabado.wav',
                                    ContentFile(file.read()))
                url = reverse('acento_nuevo_valido')
                return JsonResponse(status=302, data={'success': url})
            # No recorded Audio
            except MultiValueDictKeyError:
                return redirect('acento_nuevo_valido')
        else:
            HttpResponse('<h1>Falló subida de archivo</h1>')
    else:
        form = AcentoForm()
    return render(request, 'acentos/acento-nuevo.html',
                  {'form': form, 'acento_nvo': acento_nvo})


def acento_nuevo_valido(request):
    acento_enviado = Home_contents.objects.get(title='ANE')
    return render(request, 'acentos/acento-nuevo-enviado.html',
                  {'acento_enviado': acento_enviado})
