from django.contrib import admin
from .models import Acentos, Home_states, Home_contents, Ubicacion
from leaflet.admin import LeafletGeoAdmin


class AdminAcentos(LeafletGeoAdmin):
    list_display = ('name', 'state_mexico', 'published_revision')


class AdminUbicacion(admin.ModelAdmin):
    list_display = ('acento', 'srid', 'x', 'y')


class AdminHome_states(admin.ModelAdmin):
    list_display = ('state_mexico', 'state_description')


class AdminHome_contents(admin.ModelAdmin):
    list_display = ('title',)


class MyModelAdmin(admin.ModelAdmin):
    def log_addition(self, *args):
        return

    # Limit which users can delete.
    # TODO: update to "editor" type of users
    def get_actions(self, request):
        actions = super(MyModelAdmin, self).get_actions(request)
        if request.user.username[0].upper() != 'M':
            del actions['delete_selected']
        return actions


admin.site.register(Acentos, AdminAcentos)
admin.site.register(Ubicacion, AdminUbicacion)
admin.site.register(Home_states, AdminHome_states)
admin.site.register(Home_contents, AdminHome_contents)
admin.site.site_title = 'Acentoteca configuración'
admin.site.index_title = ''
