Django==2.0.3
django-leaflet==0.23.0
django-embed-video==1.1.2
social-auth-app-django==2.1.0
psycopg2 --no-binary :all:
django-widget-tweaks==1.4.3 
